let mapleader = ","         " Sets leader key
set background=dark         " Set background
set termguicolors           " Enable true colors support
set encoding=UTF-8          " Set encoding
set showmatch               " Show matching brackets.
set number relativenumber   " Show the relative line number with absolute number at current line
set formatoptions+=o        " Continue comment marker in new lines.
set expandtab               " Insert spaces when TAB is pressed.
set tabstop=4               " Render TABs using this many spaces.
set shiftwidth=4            " Indentation amount for < and > commands.
set clipboard+=unnamedplus  " +y to yank/copy to clipboard
set nojoinspaces            " Prevents inserting two spaces after punctuation on a join (J)
set scrolloff=20            " Set scrolloff past EOF

" More natural splits
set splitbelow          " Horizontal split below current.
set splitright          " Vertical split to right of current.

if !&scrolloff
	set scrolloff=10 "Show next 10 lines while scrolling
endif
if !&sidescrolloff
	set sidescrolloff=5 "Show next 5 columns while side-scrolling
endif

if has('nvim') || has('termguicolors')
  set termguicolors
endif

set nostartofline "Do not jump to first character with page commands

call plug#begin('~/.config/nvim/plugged/')

    """ THEMES
    Plug 'challenger-deep-theme/vim', { 'as': 'challenger-deep' }
    Plug 'joshdick/onedark.vim'

    """ GIT
    Plug 'tpope/vim-fugitive'
    Plug 'airblade/vim-gitgutter'
    Plug 'jreybert/vimagit'

    """ STATUS
    Plug 'itchyny/lightline.vim'

    """ SYNTAX
    Plug 'sheerun/vim-polyglot'
    Plug 'othree/html5.vim'
    Plug 'jelera/vim-javascript-syntax'

    """ AUTOCOMPLETION
    Plug 'slashmili/alchemist.vim'
    Plug 'tpope/vim-endwise'
    Plug 'shougo/deoplete.nvim'
    Plug 'jiangmiao/auto-pairs'
    Plug 'alvan/vim-closetag'
    Plug 'terryma/vim-multiple-cursors'

    """ GENEREAL PLUGINS
    Plug 'junegunn/fzf'

call plug#end()

" Switch panes config
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Start explorer
nmap ä :Sexplore<CR>
nmap ö :Vexplore<CR>

" Newtrw config
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_altv = 1

"Remap key to exit insert mode
:imap jj <Esc>

" FZF
nnoremap <C-q> :FZF<CR>
"map <F1> :FZF<CR>

" Open Magit
nnoremap <C-g> :Magit<CR>

" This is the default extra key bindings
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" Autoclosetag config
let g:closetag_filenames = '*.html,*.xhtml,*.phtml, *.blade.php, *.html.eex'

" Deoplete
let g:deoplete#enable_at_startup = 1

autocmd BufNewFile,BufRead *.js call SetTwoTabConfig()
autocmd BufNewFile,BufRead *.html call SetTwoTabConfig()

function SetTwoTabConfig()
    setlocal expandtab               " Insert spaces when TAB is pressed.
    setlocal tabstop=2               " Render TABs using this many spaces.
    setlocal shiftwidth=2            " Indentation amount for < and > commands.
endfunction

" Set auto renaming for tmux
autocmd BufReadPost,FileReadPost,BufNewFile,BufEnter * call system("tmux rename-window '" . expand("%:t") . "'")
autocmd VimLeave * call system("tmux setw automatic-rename")

" Lightline
set laststatus=2
let g:lightline = {
      \ 'colorscheme': 'onedark',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

syntax on
colorscheme onedark
