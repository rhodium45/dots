# Start X at login
if status is-login
    if test -z "$DISPLAY" -a $XDG_VTNR = 1
        exec startx -- -keeptty
    end
end

function doc
    firefox /usr/share/doc/arch-wiki/html/en/
end

if status is-interactive
    if ! test -z "$DISPLAY" -a $XDG_VTNR = 1
        and not set -q TMUX
        exec tmux
    end
end

source $HOME/.asdf/asdf.fish
source ~/.asdf/asdf.fish
source $HOME/.cargo/env
set -gx PATH $HOME/.local/bin $PATH
set -gx EDITOR nvim
set -gx VISUAL nvim
set -gx PATH ~/.asdf/installs/nodejs/13.3.0/.npm/bin $PATH

function fs -d "Switch tmux session"
  tmux list-sessions -F "#{session_name}" | fzf | read -l result; and tmux switch-client -t "$result"
end

function pgstart
	sudo systemctl start postgresql.service
end

function pglogin
	sudo -iu postgres
end

function rmo
    sudo pacman -Rns (pacman -Qtdq)
end

function update
    yay -Syuu
end

function ns
    nvim (fzf)
end

function serve
    iex -S mix phx.server
end
